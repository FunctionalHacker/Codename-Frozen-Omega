package fi.metropolia.frozenomega;

public interface TickListener {
	void onTick(float deltaTime);
}
